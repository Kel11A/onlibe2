from django.test import TestCase
from django.test import Client
from . import views
from django.urls import reverse, resolve

# Create your tests here.

class SearchUnitTest(TestCase):
    def test_search_url(self):
        response = Client().get('/search/')
        self.assertEqual(response.status_code,200)

    def test_search_using_right_func(self):
        found = resolve('/search/')
        self.assertEqual(found.func, views.search)

    def test_search_url_notexist(self):
        response = Client().get('/search/')
        self.assertFalse(response.status_code==404)

    def test_search_using_right_html(self):
        response = Client().get('/search/')
        self.assertTemplateUsed(response, 'search.html')
# Create your tests here.
