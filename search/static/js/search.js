$(document).ready(function(){
	$.ajax({
		type : 'GET',
		url : '/books/?format=json',
		success:function(data){
			console.log('success',data);
			var listBuku = data;

			let data_show = ''

			for(let i = 0; i< data.length; i++){
				data_show+='<div class = "col-sm-4 text-center">'
				data_show+='<div class = "container-fluid">'
				data_show+='<div class = "jumbotron">'
				data_show+='<h2><a href="/addbook/' + data[i].id  + '">'+ data[i].title + '</a></h2>'
				data_show+='<div class="flex">'
				data_show+='<p> Author      : ' + data[i].author + '</br>'
				data_show+=    'Genre       : ' + data[i].genre + '</br>'
				data_show+=    'Publisher   : ' + data[i].publisher + '</br>'
				data_show+=    'Synopsis    : ' + data[i].synopsis + '</p>'
                data_show+= '<a href="/search/add_wishlist/' + data[i].id + '" class="btn btn-danger btn-block" data-id="{{' + data[i].id + '}}" role="button"> Add to wishlist </a>'
                data_show+= '</div>'
                data_show+= '</div>'
                data_show+= '</div>'
                data_show+= '</div>'
			}

			$("#show_buku").empty().html(data_show)

		},

		error: function(){
			alert('error');
		}
	});
});