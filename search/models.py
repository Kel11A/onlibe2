from django.db import models
from django.contrib.auth.models import User
from addbook.models import AddBook

# Create your models here.
class CustomUserProfile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	wishlist = models.ManyToManyField(AddBook)

	def __str__(self):
		return self.user.username