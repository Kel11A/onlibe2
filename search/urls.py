from django.urls import path
from . import views

app_name = 'search'

urlpatterns = [
	path('', views.search, name='search'),
	path('add_wishlist/<int:id>/', views.add_to_wishlist, name='add_wishlist')
    # dilanjutkan ...
]