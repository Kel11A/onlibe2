from django.shortcuts import redirect, render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse

from .models import CustomUserProfile
from addbook.models import AddBook

# Create your views here.
# @login_required(login_url="/login/signin/")
def search(request):
	return render(request, 'search.html')

def add_to_wishlist(request, id):
	book = get_object_or_404(AddBook, pk=id)
	user_profile, created = CustomUserProfile.objects.get_or_create(user=request.user)
	user_profile.wishlist.add(book)
	return redirect('userprofile:profile')