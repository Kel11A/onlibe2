from django.test import TestCase
from django.test import Client
from django.urls import resolve
from . import views
from .models import BookReview
from django.contrib.auth.models import User

class ProfileUnitTest(TestCase):
	def setUp(self):
		self.client = Client()
		new_user = User.objects.create_user(username="TestUser", password="testpassword123")
		new_user.save()
		user = self.client.post('/login/signin/', data={'username':'TestUser', 'password':'testpassword123'})
		
		
	def test_url_profile(self):
		user = self.client.post('/login/signin/', data={'username':'TestUser', 'password':'testpassword123'})
		response = Client().get('/profile/')
		self.assertTrue(response.status_code,200)
		
	def test_template_profile(self):
		user = self.client.post('/login/signin/', data={'username':'TestUser', 'password':'testpassword123'})
		response = Client().get('/profile/')
		self.assertTemplateUsed(response, 'profile.html')
		
	def test_profile_file_render(self):
		user = self.client.post('/login/signin/', data={'username':'TestUser', 'password':'testpassword123'})
		found = resolve('/profile/')
		self.assertEqual(found.func, views.profile)

	def test_create_review(self):
		user = self.client.post('/login/signin/', data={'username':'TestUser', 'password':'testpassword123'})
		rating = "3/5"
		review = "Suka banget!"
		return BookReview.objects.create(rating = rating, review = review)

	def test_review(self):
		user = self.client.post('/login/signin/', data={'username':'TestUser', 'password':'testpassword123'})
		r = self.test_create_review()
		self.assertTrue(isinstance(r, BookReview))
		response = self.client.get('/profile/')
		self.assertEqual(response.status_code, 200)




	
		
		

# Create your tests here.
