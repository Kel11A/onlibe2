from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from search.models import CustomUserProfile

from .serializers import ReviewSerializer
from .models import BookReview

# Create your views here.
# @login_required(login_url="/login/signin/")
def profile(request):
    context ={}
    if request.user.is_authenticated:
        user, created = CustomUserProfile.objects.get_or_create(user=request.user)


        context['wishlist'] = user.wishlist.all()
    return render(request, 'profile.html', context)


class Book_Reviews_api(APIView):
    def get(self, request):
        reviews = BookReview.objects.all()
        serializer = ReviewSerializer(reviews, many = True)
        return Response(serializer.data)

