from django.db import models
from addbook.models import AddBook


class BookReview(models.Model):
    book = models.ForeignKey(AddBook, on_delete=models.CASCADE, null=True)
    rating = models.CharField(max_length=4)
    review = models.TextField('review')
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    modified_at = models.DateTimeField(auto_now=True, editable=False)

    def __str__(self):
        return str(self.id)
# Create your models here.s
