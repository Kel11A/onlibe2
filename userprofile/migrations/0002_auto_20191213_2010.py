# Generated by Django 2.2.7 on 2019-12-13 13:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('addbook', '0001_initial'),
        ('userprofile', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bookreview',
            name='title',
        ),
        migrations.AddField(
            model_name='bookreview',
            name='book',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='addbook.AddBook'),
        ),
    ]
