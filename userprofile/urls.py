from django.contrib import admin
from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'userprofile'

urlpatterns = [
    path('', views.profile, name='profile'),
    # dilanjutkan ...
]