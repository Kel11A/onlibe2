from django import forms
from addbook.models import AddBook
from .models import BookReview

class BookReviewForm(forms.ModelForm):

    rating = forms.CharField(max_length=4)
    review = forms.CharField(widget=forms.Textarea, required=True)
    class Meta:
        model = BookReview
        fields = ['rating', 'review']
        widgets = {'review': forms.Textarea()}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })