from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import resolve
from . import views

# Create your tests here.
class OnlibeHomepageUnitTest(TestCase):

	def setUp(self):
		self.client = Client()

	def test_homepage_url_exist(self):
		response = self.client.get('/')
		self.assertEqual(response.status_code, 200)

	def test_homepage_calling_correct_views_function(self):
		found = resolve('/')
		self.assertEqual(found.func, views.homepage_view)

	def test_homepage_using_correct_template(self):
		response = self.client.get('/')
		self.assertTemplateUsed(response, 'homepage_onlibe2.html')

	def test_homepage_contains_correct_html(self):
		response = self.client.get('/')
		html_response = response.content.decode('utf8')
		self.assertIn('YOUR BOOK IS CALLING!', html_response)

	def test_homepage_using_correct_staticfiles(self):
		response = self.client.get('/')
		self.assertContains(response, 'static/css/homepage_style')