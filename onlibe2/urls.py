"""onlibe2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from addbook import views
from userprofile.views import Book_Reviews_api
from about.views import AboutList
from addbook.views import BookList


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('homepage.urls')),
    path('login/', include('login.urls')),
    path('profile/',include('userprofile.urls')),
    path('search/', include('search.urls')),
    path('addbook/', include('addbook.urls')),
    path('about/', include('about.urls')),
    path('books/', views.BookList.as_view()),
    path('bookreviews/', Book_Reviews_api.as_view()),
    path('aboutapi/', AboutList.as_view()),
    path('books/', BookList.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)



