from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import login, logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.decorators import login_required

# Create your views here.
def signin_view(request):
	if request.method == 'POST':
		form = AuthenticationForm(data=request.POST)
		if form.is_valid():
			user = form.get_user()
			login(request, user)
			return redirect('userprofile:profile')
	else:
		form = AuthenticationForm()
	return render(request, 'signin_onlibe2.html', {'form':form})

def signup_view(request):
	if request.method == 'POST':
		form = UserCreationForm(data=request.POST)
		if form.is_valid():
			username = form.cleaned_data.get('username')
			user = form.save()
			login(request, user)
			return redirect('userprofile:profile')
	else:
		form = UserCreationForm()
	return render(request, 'signup_onlibe2.html', {'form':form})

@login_required(login_url="/login/signin/")
def logout_view(request):
	# if request.method == 'POST':
		logout(request)
		return redirect('login:signin')
