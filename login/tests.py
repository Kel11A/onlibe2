from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import resolve
from django.contrib.auth.models import User
from . import views

# Create your tests here.
class OnlibeLoginUnitTest(TestCase):

	def setUp(self):
		self.client = Client()
		new_user = User.objects.create_user(username="TestUser",
											password="test_password_12345")
		new_user.save()

	def test_signin_url_exist(self):
		response = self.client.get('/login/signin/')
		self.assertEqual(response.status_code, 200)

	def test_signup_url_exist(self):
		response = self.client.get('/login/signup/')
		self.assertEqual(response.status_code, 200)

	def test_signin_calling_correct_views_function(self):
		found = resolve('/login/signin/')
		self.assertEqual(found.func, views.signin_view)

	def test_signup_calling_correct_views_function(self):
		found = resolve('/login/signup/')
		self.assertEqual(found.func, views.signup_view)

	def test_signin_using_correct_template(self):
		response = self.client.get('/login/signin/')
		self.assertTemplateUsed(response, 'signin_onlibe2.html')

	def test_signup_using_correct_template(self):
		response = self.client.get('/login/signup/')
		self.assertTemplateUsed(response, 'signup_onlibe2.html')

	def test_signin_contains_correct_html(self):
		response = self.client.get('/login/signin/')
		html_response = response.content.decode('utf8')
		self.assertIn('SIGN IN HERE!', html_response)

	def test_signup_contains_correct_html(self):
		response = self.client.get('/login/signup/')
		html_response = response.content.decode('utf8')
		self.assertIn('SIGN UP HERE!', html_response)

	def test_signin_using_correct_staticfiles(self):
		response = self.client.get('/login/signin/')
		self.assertContains(response, 'static/css/login_style')

	def test_signup_using_correct_staticfiles(self):
		response = self.client.get('/login/signup/')
		self.assertContains(response, 'static/css/login_style')

	def test_signin_can_login_to_user(self):
		user = self.client.post	('/login/signin/', data={
									'username':'TestUser',
									'password':'test_password_12345'
								})
		response = self.client.get('/profile/')
		self.assertEqual(response.status_code, 200)

	def test_signup_can_create_new_user(self):
		user = self.client.post	('/login/signup/', data={
									'username':'TestNewUser',
									'password1':'test_new_password_12345',
									'password2':'test_new_password_12345'
								})
		response = self.client.get('/profile/')
		self.assertEqual(response.status_code, 200)

	def test_logout_user_after_login(self):
		user = self.client.post	('/login/signin/', data={
									'username':'TestUser',
									'password':'test_password_12345'
								})
		log_out = self.client.post('/login/logout/')
		response = self.client.get('/profile/')
		self.assertEqual(response.status_code, 200)