from django.db import models

class About(models.Model):
    name = models.CharField('name', max_length = 30)
    review = models.TextField('review', max_length=140)

    def __str__(self):
        return self.review
# Create your models here.
