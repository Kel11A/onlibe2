from django.shortcuts import render
from django.shortcuts import redirect
from .models import About
from . import forms
from .forms import ReviewForm
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import AboutSerializer

class AboutList(APIView):
	def get(self, request):
		reviews = About.objects.all()
		serializer = AboutSerializer(reviews, many = True)
		return Response(serializer.data)

def about(request):
	review = About.objects.all().values().order_by('name')
	if (request.method == 'POST'):
		form = ReviewForm(request.POST)
		if (form.is_valid()):
			form.save()
			return redirect('/about/')
	else:
		form = ReviewForm()
	return render(request, 'about.html', {'testimonies': review, 'form':form})