from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import about
from .models import About
from .forms import *


# Create your tests here.
class aboutTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code,200)
    
    def test_about_func(self):
        found = resolve('/about/')
        self.assertEqual(found.func, about)
    
    def test_about_temp(self):
        found = self.client.get('/about/')
        self.assertTemplateUsed(found, 'about.html')

    def test_create_review(self):
        name = "naura"
        review = "keren banget kak"
        return About.objects.create(name = name, review = review)

        jumlah = About.objects.all().count()
        self.assertEqual(jumlah,1)

    
    def test_review(self):
        r = self.test_create_review() 
        self.assertTrue(isinstance(r, About))
        self.assertEqual(r.__str__(), r.review)
        resp = self.client.get("/about/")
        self.assertEqual(resp.status_code, 200) 
    
    def test_review_cre(self):
        form_data = {"name" : "naura","review" : "keren banget kak"}
        form = ReviewForm(data = form_data)
        self.assertTrue(form.is_valid())
        request = self.client.post('/about/', data = form_data)
        self.assertEqual(request.status_code, 302)

        response = self.client.get('/about/')
        self.assertEqual(response.status_code, 200)


