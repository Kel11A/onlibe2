from django import forms
from .models import About

class ReviewForm(forms.ModelForm):
    name = forms.CharField(required=True)
    review = forms.CharField(widget=forms.Textarea, required=True)
    class Meta:
        model = About
        fields = ['name', 'review']
        widgets = {'review': forms.Textarea()}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })