from django.urls import path
from django.contrib import admin
from addbook.views import (addbook, dynamic_lookup_view, BookList)

app_name = 'addBook'

urlpatterns = [
    path('', addbook, name='addbook'),
	path('<int:my_id>/', dynamic_lookup_view, name='books')

]
