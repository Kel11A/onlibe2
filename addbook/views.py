from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from .forms import FormAddBook
from userprofile.forms import BookReviewForm
from userprofile.models import BookReview
from .models import AddBook
from django.contrib.auth.decorators import login_required
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import AddBookSerializer


# Create your views here.
#@login_required(login_url="/login/signin/")

# Create your views here.
def addbook(request):
	form = FormAddBook()
	if request.method == "POST" :
		form = FormAddBook(request.POST)
		if form.is_valid():
			form.save()
		return redirect('/addbook/')
	return render(request, "addbook.html", {'form' : form})

def dynamic_lookup_view(request, my_id):
	review = BookReview.objects.all().values().order_by('id')
	obj = AddBook.objects.get(id=my_id)
	if (request.method == 'POST'):
		form = BookReviewForm(request.POST)
		print(form.is_valid())
		if (form.is_valid()):

			new_review = form.save()
			new_review = str(new_review)
			new_review_obj = BookReview.objects.get(id =new_review)
			new_review_obj.book = obj
			new_review_obj.save()
			return redirect('/addbook/'+str(my_id)+"/")
	else:
		form = BookReviewForm()

	context ={
		"object":obj,
		"form":form,
		"reviews":review,
	}
	return render (request, "book_detail.html", context)

class BookList(APIView):
	def get(self, request):
		books = AddBook.objects.all()
		serializer = AddBookSerializer(books, many=True)
		return Response(serializer.data)


	def post(self, request):
		serializer = AddBookSerializer(data = request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

