from django.db import models

# Create your models here.
class AddBook(models.Model):
	title = models.CharField(max_length = 100)
	author = models.CharField(max_length = 50)
	publisher = models.CharField(max_length = 50)
	genre = models.CharField(max_length = 50)
	synopsis = models.CharField(max_length = 1000)

	def __str__(self):
		return self.title
