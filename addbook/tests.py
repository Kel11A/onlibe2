from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import resolve
from . import views
from .models import AddBook

# Create your tests here.
class AddBookUnitTest(TestCase):
	def test_addbook_url_exist(self):
		response = Client().get('/addbook/')
		self.assertEqual(response.status_code, 200)

	def test_addbook_file_render(self):
		found = resolve('/addbook/')
		self.assertEqual(found.func, views.addbook)

	def test_addbook_template_exist(self):
		response = Client().get('/addbook/')
		self.assertTemplateUsed(response, 'addbook.html')

	def test_create_addbook(self):
		title = "Matahari"
		author = "Tere Liye"
		publisher = "Gramedia"
		genre = "Fiksi"
		synopsis = "Bukunya bagus"
		return AddBook.objects.create(title = title, author = author, publisher = publisher,
			genre = genre, synopsis = synopsis)

	def test_addbook(self):
		r = self.test_create_addbook()
		self.assertTrue(isinstance(r, AddBook))
		self.assertEqual(r.__str__(), r.title)
		response = self.client.get('/addbook/')
		self.assertEqual(response.status_code, 200)

	def test_addbook_using_correct_staticfiles(self):
		response = Client().get('/addbook/')
		self.assertContains(response, 'static/css/addbook_style')