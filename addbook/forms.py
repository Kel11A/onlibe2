from django import forms
from . import models

class FormAddBook (forms.ModelForm):
    title = forms.CharField(widget = forms.TextInput(attrs={
    	"size" : 100,
        "class : form-control"
        "required" : True,
        }))

    author = forms.CharField(widget = forms.TextInput(attrs={
    	"size"  : 100,	
    	"class : form-control"
        "required" : True,
        }))
    publisher = forms.CharField(widget = forms.TextInput(attrs={
    	"size" : 100,
    	"class : form-control"
        "required" : True,
        }))

    genre = forms.CharField(widget = forms.TextInput(attrs={
    	"size" : 100,
    	"class : form-control"
        "required" : True,
        }))

    synopsis = forms.CharField(widget = forms.Textarea(attrs={
    	"rows" : 4, "cols" : 100
        }))

    class Meta:
    	model = models.AddBook
    	fields = ["title", "author", "publisher", "genre", "synopsis"]
